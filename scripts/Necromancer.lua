local necromancer = {}

local apiUtils = require "SynchronyExtendedAPI.utils.APIUtils"
local entities = require "SynchronyExtendedAPI.extended.Entities"
local namedPlayers = require "SynchronyExtendedAPI.templates.NamedPlayers"
local namedItems = require "SynchronyExtendedAPI.templates.NamedItems"
local namedEnemies = require "SynchronyExtendedAPI.templates.NamedEnemies"

local commonSpell = require "necro.game.data.spell.CommonSpell"
local collision = require "necro.game.tile.Collision"
local action = require "necro.game.system.Action"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local team = require "necro.game.character.Team"
local character = require "necro.game.character.Character"
local move = require "necro.game.system.Move"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

function necromancer.register()

    components.register {
        spellcastSpawnEntity = {
            constant.string("typeName"),
            constant.int8("teamId", team.Id.PLAYER),
        },
        transformOnHit = {
            constant.string("target"),
        },
    }

    apiUtils.safeAddEvent(
        event.spellcast,
        "spawnEntity",
        {
            order = "offset",
            sequence = 1,
            filter = "ExtendedUniverse_spellcastSpawnEntity",
        },
        function (ev)
            local direction = ev.caster.facingDirection.direction ~= action.Direction.NONE
                              and ev.caster.facingDirection.direction
                              or action.Direction.RIGHT
            move.direction(ev.caster, direction, -1, move.Type.KNOCKBACK)

            local offsetX, offsetY = action.getMovementOffset(ev.caster.facingDirection.direction)
            local spawnedEntity = object.spawn(
                ev.entity.ExtendedUniverse_spellcastSpawnEntity.typeName,
                ev.caster.position.x + offsetX,
                ev.caster.position.y + offsetY,
                {
                    dropCurrencyOnDeath = {active = false},
                    killCredit = {active = false},
                    team = {
                        id = ev.entity.ExtendedUniverse_spellcastSpawnEntity.teamId == team.Id.PLAYER
                             and ev.caster.team.id
                             or ev.entity.ExtendedUniverse_spellcastSpawnEntity.teamId,
                        },
                })

            object.moveToNearbyVacantTile(spawnedEntity)
            character.setFacingDirection(spawnedEntity, direction)
        end)

    -- Necromancer character
    entities.addPlayer(namedPlayers.Eli, {
            name = "Necromancer",

            components = {
                {
                    friendlyName = "Necromancer",
                    playableCharacter = {
                        lobbyOrder = 101,
                    },
                    sprite = {
                        texture = "mods/ExtendedUniverse/gfx/Necromancer/Necromancer_body.png",
                    },
                    characterEquipmentSpriteRow = false,
                    bestiary = {
                        image = "mods/ExtendedUniverse/gfx/Necromancer/Necromancer_bestiary.png",
                        focusX = 276,
                        focusY = 166,
                    },
                    initialEquipment = {
                        items = {
                            "WeaponEli",
                            "ShovelBasic",
                            "ExtendedUniverse_SpellSummonAttacker",
                            "ExtendedUniverse_SpellSummonDefender",
                        },
                    },
                },
                {
                    sprite = {
                        texture = "mods/ExtendedUniverse/gfx/Necromancer/Necromancer_head.png",
                    },
                    characterEquipmentSpriteRow = false,
                },
            },
        })

    -- Summon Attacker spell
    entities.addItem(nil, {
        name = "SpellSummonAttacker",

        data = {
            slot = "spell",
            flyaway = "Summon Attacker",
            hint = "Summons an attacker.",
            isSpell = true,
        },

        components = {
            sprite = {
                texture = "ext/entities/zombie.png",
            },
            itemCastOnUse = {
                spell = "ExtendedUniverse_SpellcastSummonAttacker",
            },
            spellCooldownTime = {
                cooldown = 8,
            },
            spellUseCasterFacingDirection = {},
        },
    })

    entities.addEnemy(
        namedEnemies.Zombie,
        {
            name = "UndeadAttacker",
            data = {
                friendlyName = "Undead attacker",
            },
            components = {
                beatDelay = {
                    counter = 1,
                    interval = 1,
                },
                team = {
                    id = team.Id.NONE,
                },
            },
        })

    apiUtils.safeCall(
        function() 
            commonSpell.registerSpell("SpellcastSummonAttacker", {
                spellcast = {
                },
                spellcastLinear = {
                    collisionMask = collision.Type.WALL,
                    minDistance = 1,
                    maxDistance = 1,
                },
                soundSpellcast = {
                    sound = "spellGeneral",
                },
                friendlyName = {
                    name = "Summon Attacker",
                },
                ExtendedUniverse_spellcastSpawnEntity = {
                    typeName = "ExtendedUniverse_UndeadAttacker",
                    teamId = team.Id.NONE,
                },
            })
        end)

    -- Summon Defender spell
    entities.addItem(nil, {
        name = "SpellSummonDefender",

        data = {
            flyaway = "Summon Defender",
            hint = "Summons a Defender.",
            slot = "spell",
        },

        components = {
            isSpell = true,
            sprite = {
                texture = "mods/ExtendedUniverse/gfx/Necromancer/defender.png",
            },
            spriteSheet = {
                frameX = 3,
                frameY = 1,
            },
            itemCastOnUse = {
                spell = "ExtendedUniverse_SpellcastSummonDefender",
            },
            spellCooldownTime = {
                cooldown = 8,
            },
            spellUseCasterFacingDirection = {},
        },
    })

    entities.addEnemy(
        namedEnemies.Armoredskeleton3,
        {
            name = "UndeadDefender",
            data = {
                friendlyName = "Undead defender",
            },
            components = {
                beheadable = false,
                lowHealthConvert = false,
                sprite = {
                    texture = "mods/ExtendedUniverse/gfx/Necromancer/defender.png",
                },
                health = {
                    health = 1,
                    maxHealth = 1,
                },
                innateAttack = {
                    damage = 0,
                },
                beatDelay = {
                    counter = 1,
                },
                shieldKnockbackOnHit = false,
                castOnDeath = { spell = "SpellcastExplosion" },
                shieldBreakOnHit = {
                    minimumDamage = 100,
                },
                team = {},
                targetNearestPlayer = false,
                targetNearestHostileEntity = {},
            },
        })

    apiUtils.safeCall(
        function()
            commonSpell.registerSpell("SpellcastSummonDefender", {
                spellcast = {
                },
                spellcastLinear = {
                    collisionMask = collision.Type.WALL,
                    minDistance = 1,
                    maxDistance = 1,
                },
                soundSpellcast = {
                    sound = "spellGeneral",
                },
                friendlyName = {
                    name = "Summon Defender",
                },
                ExtendedUniverse_spellcastSpawnEntity = {
                    typeName = "ExtendedUniverse_UndeadDefender",
                    teamId = team.Id.PLAYER,
                },
            })
        end)

    apiUtils.safeAddEvent(
        event.objectTakeDamage,
        "transformDefender",
        {
            order = "death",
            sequence = 1,
            filter = "ExtendedUniverse_transformOnHit"
        },
        function (ev)
            if ev.survived then
                ev.entity.team.id = team.Id.NONE
                object.convert(ev.entity, ev.entity.ExtendedUniverse_transformOnHit.target)
            end
        end)
end

return necromancer


