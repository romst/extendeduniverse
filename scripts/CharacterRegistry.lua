local gunslinger = require "ExtendedUniverse.Gunslinger"
local necromancer = require "ExtendedUniverse.Necromancer"
local knight = require "ExtendedUniverse.Knight"

gunslinger.register()
necromancer.register()
knight.register()