local knight = {}

local apiUtils = require "SynchronyExtendedAPI.utils.APIUtils"
local entities = require "SynchronyExtendedAPI.extended.Entities"
local namedPlayers = require "SynchronyExtendedAPI.templates.NamedPlayers"

local event = require "necro.event.Event"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

function knight.register()
    components.register {
        itemChargeDig = {},
        itemChargeAttack = {},
    }

    -- Necromancer character
    entities.addPlayer(namedPlayers.Bolt, {
        name = "Knight",
        components = {
            {
                team = {
                    id = team.Id.PLAYER,
                },
                playableCharacter = {
                    lobbyOrder = 102,
                },
                health = {
                    health = 2,
                    maxHealth = 2
                },
                positionalSprite = {
                    offsetX = -6, offsetY = -9,
                },
                sprite = {
                    texture = "mods/ExtendedUniverse/gfx/Knight/Knight_body.png",
                    width = 36,
                    height = 42,
                },
                characterEquipmentSpriteRow = false,
                bestiary = {
                    --image = "mods/ExtendedUniverse/gfx/Knight/Knight_bestiary.png",
                    image = "ext/bestiary/bestiary_yellowskeletonknight.png",
                    focusX = 80,
                    focusY = 260,
                },
                initialEquipment = {
                    items = {
                        "Bomb",
                        "ExtendedUniverse_WeaponChargeLance",
                        "ShovelBasic",
                    },
                },
                rhythmSubdivision = {
                    factor = 2,
                },
            },
            {
                -- Disable head
                sprite = false,
                characterEquipmentSpriteRow = false,
            }
        },
    })
    
    -- Register Lance
    entities.addItem(nil, {
        name = "WeaponChargeLance",

        data = {
            flyaway = "Lance",
            hint = "Deal damage based on charge-level.",
            slot = "weapon",
        },

        components = {
            sprite = {
                texture = "mods/ExtendedUniverse/gfx/Knight/weapon_lance.png",
            },
            weapon = {
                -- modified via chargeLevel
                damage = 0,
            },
            weaponPattern = {
                pattern = commonWeapon.pattern {
                    swipe = "rapier",
                    passWalls = false,
                    multiHit = false,
                    repeatTiles = 100,
                    tiles = {
                        {
                            offset = {1, 0},
                        },
                    },
                },
            },
            itemConditionalInvincibility = {},
            itemConditionalInvincibilityOnKill = {},
            itemDashOnKill = {
                moveType = move.Type.SLIDE,
            },
            ExtendedUniverse_itemChargeAttack = {},
        },
    })

    -- Charge events --
    apiUtils.safeAddEvent(
        event.weaponAttack,
        "applyChargeAttack",
        {
            order = "pattern",
            sequence = 1,
            filter = {
                "weaponPattern",
                "ExtendedUniverse_itemChargeAttack"
            }
        }, function (ev)
        if ev.pattern and action.isOrthogonalDirection(ev.direction) then
            attack.applyDirectionalAttack(ev.result, ev.attacker, ev.weapon, ev.pattern, ev.direction)

            if ev.result.success then
                local victim = ev.result.targets[1].victim

                local dx, dy = action.getMovementOffset(ev.result.targets[1].direction)

                local distanceX = math.abs(victim.position.x - ev.attacker.position.x)
                local distanceY = math.abs(victim.position.y - ev.attacker.position.y)

                ev.result.dx = dx * (distanceX - 1)
                ev.result.dy = dy * (distanceY - 1)

                ev.result.targets[1].damage = math.max(distanceX, distanceY)
            end
        end
    end)

    -- Register charge damage
    apiUtils.safeAddEvent(
        event.holderDealDamage,
        "chargeAttack",
        {
            order = "applyDamage",
            filter = "ExtendedUniverse_itemChargeAttack",
        },
        function (ev)
            if ev.holder:hasComponent("ExtendedUniverse_chargeEntity") then
                -- frontal invulnerability
                ev.knockback = 1

                -- set 'DashOnKill' to active to simulate 'DashOnKnockback'
                ev.entity.itemDashOnKill.active = true

                -- deal directly (move on tile of killed enemy)
                ev.damage = ev.holder.ExtendedUniverse_chargeEntity.chargeLevel
            end
        end)

end

return knight