local gunslinger = {}

local abs = math.abs

local apiUtils = require "SynchronyExtendedAPI.utils.APIUtils"
local entities = require "SynchronyExtendedAPI.extended.Entities"
local namedPlayers = require "SynchronyExtendedAPI.templates.NamedPlayers"
local namedItems = require "SynchronyExtendedAPI.templates.NamedItems"

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local event = require "necro.event.Event"
local collision = require "necro.game.tile.Collision"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

function gunslinger.register()
    components.register {
        weaponSwipeLimitedTrailRayCast = {},
    }

    entities.addPlayer(namedPlayers.Cadence, {
        name = "Gunslinger",
        data = {},
        components = {
            playableCharacter = {
                lobbyOrder = 103,
            },
            sprite = {
                texture = "ext/entities/player2_armor_body.png",
            },
            bestiary = {
                image = "ext/bestiary/bestiary_clone.png",
                focusX = 236,
                focusY = 156,
            },
            initialEquipment = {
                items = {
                    "ExtendedUniverse_Revolver",
                    "ShovelBasic",
                },
            },
        },
    })


    entities.addItem(namedItems.WeaponRifle, {
        name = "Revolver",
        
        data = {
            flyaway = "Revolver",
            hint = "Deal damage from a distance.",
            slot = "weapon",
        },

        components = {
            sprite = {
                texture = "mods/ExtendedUniverse/gfx/Gunslinger/weapon_revolver.png",
                width = 24,
                height = 24,
            },
            weaponReloadable = {
                pattern = commonWeapon.pattern {
                    passWalls = false,
                    multiHit = false,
                    forceAttack = true,
                    repeatTiles = 4,
                    tiles = {
                        {
                            offset = {1, 0},
                            attackFlags = attack.mask(attack.Flag.PROVOKE, attack.Flag.DIRECT),
                            damageMultiplier = 1,
                        },
                        damageMultiplier = 1,
                    },
                },
                maximumAmmo = 6,
                ammoPerReload = 6,
            },
            weaponSwipeTrailRayCast = false,
            ExtendedUniverse_weaponSwipeLimitedTrailRayCast = {},
        },
    })

    local function addSwipeTrail(ev, distance, type)
        for i = 1, distance do
            ev.result.swipes[#ev.result.swipes + 1] = {
                type = type,
                data = {offset = i},
                weapon = ev.weapon,
            }
        end
    end

    local function getDirectionOffset(direction)
        local dx, dy = action.getMovementOffset(direction)
        return (dx ~= 0) and dx / abs(dx) or 0, (dy ~= 0) and dy / abs(dy) or 0
    end

    local function checkReach(position, direction, maxDistance)
        local x, y = position.x, position.y
        local dx, dy = getDirectionOffset(direction)
        for i = 1, maxDistance, 1 do
            if
                collision.check(x + dx * i, y + dy * i, collision.Type.CHARACTER)
                or collision.check(x + dx * i, y + dy * i, collision.Type.OBJECT)
            then
                return i
            elseif collision.check(x + dx * i, y + dy * i, collision.Type.WALL) then
                return i - 1
            end
        end

        return maxDistance
    end

    apiUtils.safeAddEvent(
        event.weaponAttack,
        "revolverRaycast",
        {
            order = "swipe",
            filter = "ExtendedUniverse_weaponSwipeLimitedTrailRayCast",
            sequence = 1,
        }, function (ev)
            if ev.result.success and ev.loadedAttack then
                addSwipeTrail(ev, checkReach(ev.attacker.position, ev.direction, 4), "trail")
            end
        end)
end

return gunslinger